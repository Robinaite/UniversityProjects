/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.kitchen;

import eapli.ecafeteria.domain.meals.Meal;
import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 *
 * @author Robin Couwenberg
 */
@Entity
public class MealItem implements Serializable {
    
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /**
     * The Meal
     */
    @OneToOne
    private Meal meal;
    
    private PlannedMealQuantity quantity;
    //for jpa purposes
    protected MealItem() {
    }

    /**
     * Constructing the meal item requires it's respective meal
     * @param meal - the meal it refers to
     */
    public MealItem(Meal meal) {
        this.meal = meal;
        this.quantity = new PlannedMealQuantity();
    }
    public Meal  obtainMeal(){
        return meal;
    }
    /**
     * Changes the planned quantity
     * @param qnt the quantity to change to;
     * @return true -> value was accepted, false -> not able to change the planned quantity
     */
    public boolean changePlannedMealQuantity(int qnt){
        return quantity.setPlannedQuantity(qnt);
    }

    @Override
    public String toString() {
        return "Meal: " + meal.toStringWithDish()
                + "\n" + quantity;
    }
    
}
