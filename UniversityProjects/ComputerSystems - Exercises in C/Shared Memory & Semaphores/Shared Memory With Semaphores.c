#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <semaphore.h>



#define MAX_VISITORS_SEM 0
#define SHM_SEM 1
#define OPEN 1
#define CLOSED 0
int main (void){

    const int QTD_VISITORS = 8;
    const int QTD_SEM = 2;
    const int WAITING_TIMEOUT = 5;
    const int TIME_TILL_OPEN = 1; //(In seconds) Needs to be less than waiting timeout, or else the visitors will be gone before the showroom even opens
    const int TIME_CLOSE = 10; //In Seconds
    char sems_names[][25] = {"/sem1","/sem2"};
    char *shm_name = "/shm15";
    int sem_init_values[] = {5,1};
    int i,fd,data_size = sizeof(int);

    sem_t * sem[QTD_SEM];

    //creates semaphores
    for(i = 0; i<QTD_SEM;i++){
        sem[i] = sem_open(sems_names[i],O_CREAT|O_EXCL,0644,sem_init_values[i]);
        if(sem[i] == SEM_FAILED){
            perror("Unable to open semaphore.");
            exit(-1);
        }
    }

    for(i = 0;i<QTD_VISITORS;i++){
        pid_t pid = fork();
        if(pid==-1){
            perror("Fork Failed!");
            exit(-1);
        }
        if(pid==0){//filhos

            fd = shm_open(shm_name,O_CREAT|O_RDWR,S_IRUSR|S_IWUSR);
            if(fd == -1){
                perror("SHM_OPEN Failed:!\n");
                exit(-1);
            }

            if(ftruncate(fd,data_size)==-1){
                perror("Unable to ftruncate!");
                exit(-1);
            }
            

            int* showRoomState = (int*)mmap(NULL,data_size,PROT_READ | PROT_WRITE,MAP_SHARED,fd,0);
            if(showRoomState == MAP_FAILED){
                perror("Unable to map from shared memory!");
                exit(-1);
            }

            if(i==0){//filho 0 = Showroom opening/closing.
                sem_wait(sem[SHM_SEM]);
                *showRoomState = CLOSED;
                sem_post(sem[SHM_SEM]);
                sleep(TIME_TILL_OPEN);
                sem_wait(sem[SHM_SEM]);
                *showRoomState = OPEN;
                printf("______________________________________________\n\nShow Room Opened\n_________________________________________________________________\n");
                fflush(stdout);
                sem_post(sem[SHM_SEM]);
                sleep(TIME_CLOSE);

                sem_wait(sem[SHM_SEM]);
                printf("______________________________________________\n\nShow Room Closed\n______________________________________________________________\n");
                fflush(stdout);
                *showRoomState = CLOSED;
                sem_post(sem[SHM_SEM]);
            } else {//visitors
                int times = 0;
                while(times<WAITING_TIMEOUT){ //Supposedly a infinite loop of a visitor entering and leaving. 
                    sem_wait(sem[SHM_SEM]);
                    if(*showRoomState == OPEN){
                        sem_post(sem[SHM_SEM]);
                        if(sem_trywait(sem[MAX_VISITORS_SEM])==0){ //check if able to enter, not blocking cause need to repeatedly check if showroom is still opened
                            printf("Visitor %d: Entered Show Room.\n",i);
                            fflush(stdout);
                            sleep(2);
                            sem_post(sem[MAX_VISITORS_SEM]);//leaves the showroom;
                            printf("Visitor %d: Left Show Room.\n",i);
                            fflush(stdout);
                            times = 0;//resets the timer to leave
                        } 
                    } else if(*showRoomState == CLOSED){
                        sem_post(sem[SHM_SEM]);
                        printf("Visitor %d: Show Room Is Closed. Leaving in %d seconds.\n",i,WAITING_TIMEOUT-times);
                        fflush(stdout);
                        times++; //Timeout of WAITING_TIMEOUT in seconds to wait if the room to open.
                        sleep(1);
                    } else {
                        sem_post(sem[SHM_SEM]);
                    }
                }
            }
            if(munmap(showRoomState,data_size) == -1){
                perror("Unable to unmap");
                exit(-1);
            }

            if(close(fd)==-1){
                perror("Unable to close");
                exit(-1);
            }

            exit(1);
        }
    }

    for(i = 0; i<QTD_VISITORS;i++){
        wait(NULL);
    }

    for(i = 0; i<QTD_SEM;i++){
        if(sem_close(sem[i])==-1){
            perror("Unable to close sem.\n");
            exit(-1);
        }
        if(sem_unlink(sems_names[i])==-1){
            perror("Unable to unlink sem.");
            exit(-1);
        }
    }
    

    return 0;
}