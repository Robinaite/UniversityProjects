/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.kitchen;

import eapli.ecafeteria.domain.dishes.Dish;
import eapli.ecafeteria.domain.kitchen.MealItem;
import eapli.ecafeteria.domain.kitchen.MealsPlan;
import eapli.ecafeteria.domain.mealmenu.MealMenu;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.MealsPlanRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Robin Couwenberg
 */
public class ElaborateMealsPlanFromMenuController {
    
    private MealsPlan selectedMealsPlan;
    private MealMenu selectedMenu;
    private MealsPlanRepository mealsPlanRepo;
    private CloseMealsPlanController cmpController;

    public ElaborateMealsPlanFromMenuController() {
        mealsPlanRepo = PersistenceContext.repositories().mealsPlans();
        cmpController = new CloseMealsPlanController();
    }
    
    
     /**
     * Obtains all the available menus to choose from to create a meals plan
     * @return an iterable of all the available menu's
     */
    public Iterable<MealMenu> obtainAvailableMenus(){
        ListMenusController availableMenusCtrl = new ListMenusController();
        return availableMenusCtrl.allAvailableMenus();
    }
    /**
     * Selects the menu to create a meals plan from
     * @param menu the menu to create a meals plan from
     */
    public void selectMenu(MealMenu menu){
        this.selectedMenu = menu;
    }
    /**
     * Create meals plan from selected menu
     */
    public void createMealsPlan(){
        MealsPlan plan = new MealsPlan(selectedMenu);
        Iterable<Meal> mealsFromMenu = selectedMenu.getMealsFromMenu();
        for(Meal meal : mealsFromMenu){
            MealItem mealItem = new MealItem(meal);
            plan.addMealItem(mealItem);
        }
        selectedMealsPlan = plan;        
    }
         /**
     * Obtains all the available meals plan to choose from
     * @return an iterable of all the available meals plan's
     */
    public Iterable<MealsPlan> getMealsPlanAvailableToEdit(){
       
     
        for(MealsPlan mp: mealsPlanRepo.findAllOpenPlans()){
            cmpController.closeMealsPlan(mp);
            selectMealsPlan(mp);
            saveMealsPlan();
        }
        return mealsPlanRepo.findAllOpenPlans();
    }
    /**
     * Selects the meals plan to edit the meal items from
     * @param plan 
     */
    public void selectMealsPlan(MealsPlan plan){
        this.selectedMealsPlan = plan;
    }
    
    /**
     * Obtains all mealitems from meals plan to be edited
     * @return 
     */
    public Iterable<MealItem> getMealItemsFromMealsPlan(){
        return selectedMealsPlan.getMealItems();
    }
    /**
     * Change the planned quantity of the meal item
     * @param qnt the planned quantity to change to
     * @param item the meal item to change quantity from.
     * @return true if successful, false if not valid quantity.
     */
    public boolean changePlannedQuantity(int qnt, MealItem item){
        return item.changePlannedMealQuantity(qnt);
    }
    /**
     * Saves the edited meals plan.
     * @return true, if saved worked. False if error occurs.
     */
    public boolean saveMealsPlan(){
        try {
            mealsPlanRepo.save(selectedMealsPlan);
        } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
}
