/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.app.backoffice.console.presentation.kitchen;

import eapli.ecafeteria.application.kitchen.ElaborateMealsPlanFromMenuController;
import eapli.ecafeteria.domain.kitchen.MealItem;
import eapli.ecafeteria.domain.kitchen.MealsPlan;
import eapli.ecafeteria.domain.mealmenu.MealMenu;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import java.util.Iterator;

/**
 *
 * @author Robin Couwenberg
 */
public class EditMealsPlanUI extends AbstractUI {
    
    private final ElaborateMealsPlanFromMenuController controller = new ElaborateMealsPlanFromMenuController();
    private final int EXIT_OPTION = 0;
    
    @Override
    protected boolean doShow() {  
        Iterable<MealsPlan> plans = controller.getMealsPlanAvailableToEdit();
        int mealsPlanID = 1;
        System.out.println("Meal Plans: \n ----------------------------------------------------------- ");
        for(MealsPlan plan : plans){
            System.out.println(mealsPlanID + ". " + plan.toString());
            mealsPlanID++;
        }
          System.out.println("----------------------------------------------------------- ");
        int option = Console.readInteger("Which Meal Plan do you want to choose(Write 0 to exit)?");
        if(option == EXIT_OPTION){
            return true;
        }
        int id = 0;
        Iterator it = plans.iterator();
        while(it.hasNext() && id<option-1){
            it.next();
            id++;
        }
        MealsPlan selectedPlan = (MealsPlan) it.next();
        controller.selectMealsPlan(selectedPlan);
                
        Iterable<MealItem> mealItems = controller.getMealItemsFromMealsPlan();
        
        for(MealItem item : mealItems){
            System.out.println(item.toString());
             int changedQnt = Console.readInteger("What is the planned quantity for this meal?");
              boolean changed = item.changePlannedMealQuantity(changedQnt);
              while(changed == false){
                  changedQnt = Console.readInteger("ERROR: Not a valid quantity inserted! \nWhat is the planned quantity for this meal?");
                  changed = item.changePlannedMealQuantity(changedQnt);
              }          
        }
        System.out.println("Saving Meals Plan!");
        if(controller.saveMealsPlan()==true){
            System.out.println("Success!");
            return true;
        } 
        System.out.println("Failure in saving!");   
        return true;
    }

    @Override
    public String headline() {
        return "Edit Meals Plan";
    }
    
}
