#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <semaphore.h>


#define CAP_SEM 0
#define SHM_SEM 1
#define TRUE 1
#define FALSE 0
typedef struct{
    int nextVIPNum;
    int nextSpecialNum;
    int nextNormalNum;
    int currentVIPNum;
    int currentSpecialNum;
    int currentNormalNum;
    int numberPeople;
}Cinema;
int main (void){ 

    const int QTD_VISITORS = 20;
    const int QTD_SEM = 2;
    const int MAX_CAP = 10;
    char sems_names[][25] = {"/sem1","/sem2"};
    char *shm_name = "/shm17";
    int sem_init_values[] = {MAX_CAP,1};
    int i,fd,data_size = sizeof(int);

    sem_t * sem[QTD_SEM];

    //creates semaphores
    for(i = 0; i<QTD_SEM;i++){
        sem[i] = sem_open(sems_names[i],O_CREAT|O_EXCL,0644,sem_init_values[i]);
        if(sem[i] == SEM_FAILED){
            perror("Unable to open semaphore.");
            exit(-1);
        }
    }

    for(i = 0;i<QTD_VISITORS;i++){
        pid_t pid = fork();
        if(pid==-1){
            perror("Fork Failed!");
            exit(-1);
        }
        if(pid==0){//each process is 1 visitor

            fd = shm_open(shm_name,O_CREAT|O_RDWR,S_IRUSR|S_IWUSR);
            if(fd == -1){
                perror("SHM_OPEN Failed:!\n");
                exit(-1);
            }

            if(ftruncate(fd,data_size)==-1){
                perror("Unable to ftruncate!");
                exit(-1);
            }
            Cinema* cinema = (Cinema*)mmap(NULL,data_size,PROT_READ | PROT_WRITE,MAP_SHARED,fd,0);
            if(cinema == MAP_FAILED){
                perror("Unable to map from shared memory!");
                exit(-1);
            }
            
            if(cinema->numberPeople < MAX_CAP){ //if the cinema is not full for the first time allow everyone to enter without order
                sem_wait(sem[CAP_SEM]);   
                sem_wait(sem[SHM_SEM]);             
                cinema->numberPeople++;
                sem_post(sem[SHM_SEM]);
                printf("V %d: Entered the cinema.\n",i);
                sleep(2);
                sem_post(sem[CAP_SEM]);
                printf("V %d: Left the cinema.\n",i);
            } else { //If the cinema got full once only enter based on order.
                int myTicketNumber;
                if(i % 3 == 0){ //VIP People
                    sem_wait(sem[SHM_SEM]);
                    cinema->nextVIPNum++;
                    myTicketNumber = cinema->nextVIPNum; //Obtain ticket for order
                    sem_post(sem[SHM_SEM]);
                    printf("V %d(VIP): Waiting to enter the cinema.\n",i);
                    while(1){
                        sem_wait(sem[CAP_SEM]);
                        sem_wait(sem[SHM_SEM]);
                        if(myTicketNumber != cinema->currentVIPNum){
                            sem_post(sem[SHM_SEM]);
                            sem_post(sem[CAP_SEM]);
                        } else {
                            sem_post(sem[SHM_SEM]);
                            break; //Enters the cinema;
                        }
                    }
                    printf("V %d(VIP): Entered the cinema.\n",i);
                } else if(i % 3 == 1){ //Special people
                    sem_wait(sem[SHM_SEM]);
                    cinema->nextSpecialNum++;
                    myTicketNumber = cinema->nextSpecialNum;
                    sem_post(sem[SHM_SEM]);
                    printf("V %d(Special): Waiting to enter the cinema.\n",i);
                    while(1){
                        sem_wait(sem[CAP_SEM]);
                        sem_wait(sem[SHM_SEM]);
                        if(myTicketNumber != cinema->currentSpecialNum){
                            sem_post(sem[SHM_SEM]);
                            sem_post(sem[CAP_SEM]);
                        } else {
                            sem_post(sem[SHM_SEM]);
                            break;
                        }
                    }
                    printf("V %d(Special): Entered the cinema.\n",i);
                } else { //Normal People
                    sem_wait(sem[SHM_SEM]);
                    cinema->nextNormalNum++;
                    myTicketNumber = cinema->nextNormalNum;
                    sem_post(sem[SHM_SEM]);
                    printf("V %d(Normal): Waiting to enter the cinema.\n",i);
                    while(1){
                        sem_wait(sem[CAP_SEM]);
                        sem_wait(sem[SHM_SEM]);
                        if(myTicketNumber != cinema->currentNormalNum){
                            sem_post(sem[SHM_SEM]);
                            sem_post(sem[CAP_SEM]);
                        } else {
                            sem_post(sem[SHM_SEM]);
                            break;
                        }
                    }
                    printf("V %d(Normal): Entered the cinema.\n",i);
                }
                sleep(2); //Time Before the person leaves the room;
                sem_wait(sem[SHM_SEM]);
                if(cinema->nextVIPNum > cinema->currentVIPNum){ //Sets the order of the person who goes next
                    cinema->currentVIPNum++;
                } else if(cinema->nextSpecialNum > cinema->currentSpecialNum){
                    cinema->currentSpecialNum++;
                } else {
                    cinema->currentNormalNum++;
                }
                sem_post(sem[SHM_SEM]);
                sem_post(sem[CAP_SEM]); //leaves the cinema
                printf("V %d: Left the cinema.\n",i);
            }


            if(munmap(cinema,data_size) == -1){
                perror("Unable to unmap");
                exit(-1);
            }

            if(close(fd)==-1){
                perror("Unable to close");
                exit(-1);
            }

            exit(1);
        }
    }
    fd = shm_open(shm_name,O_CREAT|O_RDWR,S_IRUSR|S_IWUSR);
    if(fd == -1){
        perror("SHM_OPEN Failed:!\n");
        exit(-1);
    }

    if(ftruncate(fd,data_size)==-1){
        perror("Unable to ftruncate!");
        exit(-1);
    }
    Cinema* cinema = (Cinema*)mmap(NULL,data_size,PROT_READ | PROT_WRITE,MAP_SHARED,fd,0);
    if(cinema == MAP_FAILED){
        perror("Unable to map from shared memory!");
        exit(-1);
    }
    while(cinema->numberPeople < MAX_CAP);

    sem_wait(sem[SHM_SEM]);
    cinema->currentVIPNum++; //This is to initiate the count of people entering after the cinema got full once.
    sem_post(sem[SHM_SEM]);
    
    if(munmap(cinema,data_size) == -1){
        perror("Unable to unmap");
        exit(-1);
    }

    if(close(fd)==-1){
        perror("Unable to close");
        exit(-1);
    }

    for(i = 0; i<QTD_VISITORS;i++){
        wait(NULL);
    }

    for(i = 0; i<QTD_SEM;i++){
        if(sem_close(sem[i])==-1){
            perror("Unable to close sem.\n");
            exit(-1);
        }
        if(sem_unlink(sems_names[i])==-1){
            perror("Unable to unlink sem.");
            exit(-1);
        }
    }
     if(shm_unlink(shm_name) == -1){
        perror("Unlink failed");
        exit(-1);
    }
    
    return 0;
}