**Robin Couwenberg** (1161272) - Sprint 2 - Core03.2
===============================

# 1. General Notes

*In this section you should register important notes regarding your work during the sprint. For instance, if you spend significant time helping a colleague or if you work in more than one feature increment.*

# 2. Requirements

*In this section you should describe the requirements for this sprint.*

*This is simply an example of documentation*

It should be possible to filter a range of cells. The filter should be a boolean formula that is applied to each row (and a specific column of the range). If the result is true, the row is visible. If the result is false, the row should become invisible.

After talking to the client these requirements were added:
Possibility of reseting the filters after they have been applied.

# 3. Analysis

*In this section you should describe the study/analysis/research you developed in order to design a solution.*

For this feature increment, I need to:  

- Understand how the Workbook Page is implemented (for instance, how the UI renders the spreadsheet that is displayed)  

- Understand how to obtain specific column and rows of the cells.

- Understand how to reset a table.

## 3.1 Understanding of the spreadsheet and the tables

The UI is based on the GWT Material Design Framework. The spreadsheet makes use of specifc components of the framework to show the table with all the data.

The component used to show the table is: 

MaterialDataTable<SheetCell> customTable;

Material Data Table is an extension of the basic Material Table which allows a user to create a table based on specific objects intead of primitive content.

In this case the data used in the table are SheetCells.

	class SheetCell {

        private Spreadsheet sheet;
        private int row = -1;

        public SheetCell(Spreadsheet sheet, int row) {
            this.row = row;
            this.sheet = sheet;
        }

        pt.isep.nsheets.shared.core.Cell getCell(int column) {
            return this.sheet.getCell(column, this.row);
        }

        public int getColumnCount() {
            return sheet.getRow(row).length;
        }
    }

A sheetcell is the visual representative of a row in the table.

We also have columns which are rendered by the SheetWidgetColumn. 

## 3.2 Filters

As requested by the client there's a need to filter the spreadhseet content.



### 3.2.1 Formula

The formula to filter the content will be a boolean formula, which the result may only TRUE or FALSE.

Depending on the result of the formula a row in the selected range will be hidden from view. 

- True: When the condition is true for the specific cell the row is visible
- False: Whenever the condition is false for the specific cell in the colum the row will be hidden from view.

### 3.2.2 Selection Range of Cells

To use the filters the user needs to select a range of Cells.

In this case there are two cases possible:

The users selected range is 1 column wide, example: A1:A3. In this case the selected column will the respective one.

The user selected range is more than 1 column wide, example: A1:C3. In this case the user will need to select a specific column to execute the filter on.

### 3.2.2 Removing filter

After the filter is applied the user has the possibility to remove the filters.

The spreadsheet will be reset to the original layout with all the rows and cells.

## 3.4 Analysis Diagrams

The main idea for the "workflow" of this feature increment.

**Domain Model (for this feature increment)**

Since this use case is based on the UI, there's no changes to respective Domain Models.

**System Sequence Diagrams**

![Analysis SD](analysis.png)

**System Sequence Diagrams**

![Analysis Remove Filter SD](analysis2.png)

# 4. Design

*In this section you should present the design solution for the requirements of this sprint.*


## 4.1. Tests

In this case the use case is highly dependent on the UI so there's no possibility of realizing unit tests. 

## 4.2. Requirements Realization


![SD US1](design1.png)

![SD US2](design2.png)


## 4.3. Classes

Classes:
- Workbookview
- FilterPresenter
- FilterController

## 4.4. Design Patterns and Best Practices

By memory we apply/use:  
- Singleton   
- GRASP 

# 5. Implementation

**Code Organization**  

We followed the recommended organization for packages:  
- Code should be added (when possible) inside packages that identify the group, sprint, functional area and author;
- For instance, we used **lapr4.blue.s2.core.n1161272**

# 6. Work Log

![workLog](workLog.png)








