/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.isep.nsheets.server.lapr4.blue.s1.lang.n1161272.StyleExtension.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import pt.isep.nsheets.shared.core.*;
import pt.isep.nsheets.shared.core.formula.Formula;
import pt.isep.nsheets.shared.core.formula.compiler.FormulaCompilationException;
import pt.isep.nsheets.shared.core.formula.compiler.FormulaCompiler;

/**
 * Conditional Format of the Style Extensions
 * TODO - Adding the styles of a cellAddress to change to depending the result.
 * @author Robin Couwenberg - 1161272
 */
@Entity
public class ConditionalFormat implements Serializable {
    
    private static final String STYLE_EXTENSION = "Style";
    
    @Id
    @GeneratedValue
    private Long id;
    
    private Address cellAddress;
    
    private Formula formula;
    
    /**
     * Only for ORM purposes
     */
    protected ConditionalFormat() {
    }

    /**
     * Creates a new conditional Format for the respective cellAddress
     * @param cellAddress - The cellAddress to which the conditional format belongs too
     * @param source - The expression of the conditional format which is to be evaluated.
     * @throws FormulaCompilationException 
     */
    public ConditionalFormat(Address cellAddress, String source) throws FormulaCompilationException {
        this.cellAddress = cellAddress; //Add the extensions to the respective cellAddress.
        this.formula = FormulaCompiler.getInstance().compile(new CellImpl(), source);
    }
    /**
     * Evaluate the formula of this conditional format.
     * @return The result of the formula in case it's a boolean expression, else throw error
     * @throws IllegalValueTypeException Whenever the expression used is not a conditional expression throws error.
     */
    public boolean evaluateFormula() throws IllegalValueTypeException  {
        Value val = formula.evaluate();
        return val.toBoolean();
    }

    public Address obtainCellAddress() {
        return cellAddress;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
