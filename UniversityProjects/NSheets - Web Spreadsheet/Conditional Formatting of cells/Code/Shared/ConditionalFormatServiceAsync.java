/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.isep.nsheets.shared.services.lapr4.blue.s1.lang.n1161272.StyleExtension.application;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Conditional Format Async Service
 * @author Robin
 */
public interface ConditionalFormatServiceAsync {

	void newConditionalFormatAndObtainResult(ConditionalFormatDTO dto, AsyncCallback<Boolean> callback);

}
