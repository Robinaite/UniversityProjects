/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.kitchen.MealsPlan;
import eapli.ecafeteria.persistence.MealsPlanRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.Optional;
import javax.persistence.EntityManager;

/**
 *
 * @author Diogo Barreto 1150468 + Robin Couwenberg(1161272)
 */
public class InMemoryMealsPlanRepository extends InMemoryRepository<MealsPlan, Long> implements MealsPlanRepository{

    @Override
    protected Long newKeyFor(MealsPlan entity) {
        return entity.id();
    }

    @Override
    public Optional<MealsPlan> findByID(Long id) {
        return matchOne(e -> e.id().equals(id));
    }

    @Override
    public Iterable<MealsPlan> findAllOpenPlans() {
        return match(e -> e.isOpen());
    }
    
}
