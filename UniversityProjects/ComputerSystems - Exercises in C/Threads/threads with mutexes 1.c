#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <semaphore.h>
#include <pthread.h>
#include <limits.h>

#define ARRAY_SIZE 10000
#define THREADS_NUM 6
#define SUPERMARKET_NUM 3
#define MUTEX_NUM 4
#define PRODUCTS_NUM 5
#define MUTEX_VEC1 0
#define MUTEX_VEC2 1
#define MUTEX_VEC3 2
#define MUTEX_LowestValue 3

typedef struct {
    int id_h;
    int id_p;
    int p;
} Product; 

typedef struct {
    int id_h;
    int lowestCostValue;
} LowCValue;

pthread_mutex_t mutexVec[MUTEX_NUM];
Product vec[ARRAY_SIZE];
Product vec1[ARRAY_SIZE];
Product vec2[ARRAY_SIZE];
Product vec3[ARRAY_SIZE];
int vecSize1 = 0,vecSize2 = 0,vecSize3 = 0;

LowCValue LowestValue;

void *filtrate(void * element){
    int i,threadNum = (int)element;
    int qntToSearch = (ARRAY_SIZE/THREADS_NUM);
    if(threadNum == 2){ //Adds 1 more element to the last thread because 10000/3= 333.33333 So each thread does 333, beside the last one which does 334.
        qntToSearch++;
    }

    for(i = 0;i<qntToSearch;i++){
        int pos = i+threadNum*qntToSearch;
        switch(vec[pos].id_h){ 
            case 0: //Hypermarket 1
                pthread_mutex_lock(&mutexVec[0]); //The mutex are needed cause multiple threads are able to change the vec1 content
                vec1[vecSize1] = vec[pos];
                vecSize1++;
                pthread_mutex_unlock(&mutexVec[0]);
                break;
            case 1: //Hypermarket 2
                pthread_mutex_lock(&mutexVec[1]);
                vec2[vecSize2] = vec[pos];
                vecSize2++;
                pthread_mutex_unlock(&mutexVec[1]);
                break;
            case 2: //Hypermarket 3
                pthread_mutex_lock(&mutexVec[2]);
                vec3[vecSize3] = vec[pos];
                vecSize3++;
                pthread_mutex_unlock(&mutexVec[2]);
                break;
        }
    }
    pthread_exit((void *)NULL);
}

void *computing(void * element){
    int i,threadNum = (int)element;
    int size;
    Product * vec;
    switch(threadNum){ //This switch is needed cause each thread only works on one vec.
        case 0:
            size = vecSize1;
            vec = vec1;
            break;
        case 1:
            size = vecSize2;
            vec = vec2;
            break;
        case 2:
            size = vecSize3;
            vec = vec3;
            break;
    }

    int productTotal[PRODUCTS_NUM] = {0};
    int productCount[PRODUCTS_NUM] = {0};  
    int value = 0;

    for(i = 0; i<size;i++){ //Calculates the total product count and it's respective total price.
        int productID = vec[i].id_p;        
        productTotal[productID] += vec[i].p;
        productCount[productID]++;
    }

    for(i = 0; i<PRODUCTS_NUM;i++){ //calculates the average of each products and sums it together.
        if(productCount[i] != 0){
            value += productTotal[i]/productCount[i];
        }        
    }

    pthread_mutex_lock(&mutexVec[3]);
    if(LowestValue.lowestCostValue > value){ //Checks if the current hypermarket is the one with the lowest value.
        LowestValue.lowestCostValue = value;
        LowestValue.id_h = threadNum;
    }    
    pthread_mutex_unlock(&mutexVec[3]);

    pthread_exit((void *)NULL);
}

int main (void){ 
    int i;
    pthread_t thread_id[SUPERMARKET_NUM];
    int args[SUPERMARKET_NUM];
    LowestValue.lowestCostValue = INT_MAX;

    srand(pthread_self());
    for(i = 0; i<ARRAY_SIZE;i++){ //Insert data into the array
        vec[i].p = (rand() % 100)+1;
        vec[i].id_h = (rand() % SUPERMARKET_NUM);
        vec[i].id_p = (rand() % PRODUCTS_NUM);
    }    

    for(i = 0; i<MUTEX_NUM;i++){
        pthread_mutex_init(&mutexVec[i],NULL);     
    }    
    //Filtrating
    for(i = 0; i< SUPERMARKET_NUM; i++){
        args[i] = i;
        pthread_create(&thread_id[i],NULL,filtrate,(void*)args[i]);
    }
	for(i = 0; i<SUPERMARKET_NUM;i++){
       pthread_join(thread_id[i],NULL);
    }

    //Computing
    for(i = 0; i< SUPERMARKET_NUM; i++){
        args[i] = i;
        pthread_create(&thread_id[i],NULL,computing,(void*)args[i]);
    }
	for(i = 0; i<SUPERMARKET_NUM;i++){
       pthread_join(thread_id[i],NULL);
    }

    //Printing
    printf("Hypermarket: %d \nLowest Cost value: %d \n",LowestValue.id_h+1,LowestValue.lowestCostValue);

    for(i = 0; i<MUTEX_NUM;i++){
        pthread_mutex_destroy(&mutexVec[i]);
    }
    
    return 0;    
}