/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.isep.nsheets.shared.services.lapr4.blue.s1.lang.n1161272.StyleExtension.application;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import pt.isep.nsheets.shared.core.Cell;
import pt.isep.nsheets.shared.core.IllegalValueTypeException;
import pt.isep.nsheets.shared.core.formula.compiler.FormulaCompilationException;

/**
 * Service Implementation for all controllers of ConditionalFormatting
 * @author Robin
 */
@RemoteServiceRelativePath("conditionalFormatService")
public interface ConditionalFormatService extends RemoteService {
    
    public boolean newConditionalFormatAndObtainResult(ConditionalFormatDTO dto) throws FormulaCompilationException, IllegalValueTypeException;
    
}
