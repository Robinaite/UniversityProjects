#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <time.h>

#define LEITURA 0
#define ESCRITA 1

#define STDIN 0
#define STDOUT 1

#define WORDLENGHT 20
void validate(void){
	char id[WORDLENGHT];
	char password[WORDLENGHT];
	
	char localID[]="rcomp";
	char localPassword[]="rcomp1";
	
	read(STDIN,id,WORDLENGHT);
	read(STDIN,password,WORDLENGHT);
	
	if(strcmp(id,localID)!=0){//no user
		exit(2);
	}
	if(strcmp(password,localPassword)!=0){//wrong password
		exit(1);
	}
	exit(0);// all good
}
void check_password(void){
	int fd[2];
	int aux;
	int result;
	char id[WORDLENGHT];
	char password[WORDLENGHT];	
	
	
	read(STDIN,id,WORDLENGHT);
	read(STDIN,password,WORDLENGHT);
	
	if(pipe(fd) == -1){
        perror("Pipe failed");
        exit( -1);
    }
	pid_t p = fork(); //creates the child process that will read sort fx.txt

	if(p==-1){
        perror("Fork Failed");
        exit(-1);
    }
    if(p==0){
		close(fd[ESCRITA]);
		dup2(fd[LEITURA],0);
		close(fd[LEITURA]);
		validate();
	}
	close(fd[LEITURA]);
	write(fd[ESCRITA],id,WORDLENGHT);
	write(fd[ESCRITA],password,WORDLENGHT);
	close(fd[ESCRITA]);
	
	wait(&aux);
	result=WEXITSTATUS(aux);   //get exit value 
	
	switch(result){
		case 0:
			write(STDOUT,"Password verified",WORDLENGHT);
			break;
		case 1:
			write(STDOUT,"Invalid password",WORDLENGHT);
			break;
		case 2:
			write(STDOUT,"No such user",WORDLENGHT);
			break;
		default:
			write(STDOUT,"validate error",WORDLENGHT);
	}
}


int main (void){ //The last command is the parent process, cause we need to wait for the output and other commands to be done.
    int fd[2],fd2[2];

    /* cria o pipe */
    if(pipe(fd) == -1){//filho lê, pai escreve
        perror("Pipe failed");
        return 1;
    }
	if(pipe(fd2) == -1){//filho escreve, pai lê
        perror("Pipe failed");
        return 1;
    }
    pid_t p = fork(); //creates the child process that will read sort fx.txt

	if(p==-1){
        perror("Fork Failed");
        exit(-1);
    }
    if(p==0){ 
		close(fd[ESCRITA]);
		close(fd2[LEITURA]);
		
		dup2(fd[LEITURA],0); //redirects the input to the reading directory of the pipe
		dup2(fd2[ESCRITA],1); //redirects the output to the write directly through the pipe
					
		close(fd[LEITURA]);
		close(fd2[ESCRITA]);
		
		check_password();

        exit(0);
    }    
	
	close(fd[LEITURA]);
    close(fd2[ESCRITA]);
	
	
	char id[WORDLENGHT]={0};
	char password[WORDLENGHT]={0};
	
	
	
    printf("Introduza o id: \n");
    fgets(id,WORDLENGHT, stdin);
	printf("Introduza a password: \n");
    fgets(password, WORDLENGHT, stdin);
	
	id[strlen(id)-1] = '\0';// replace \n from string
	password[strlen(password)-1] = '\0';// replace \n from string
	
	write(fd[ESCRITA],id,WORDLENGHT);
	write(fd[ESCRITA],password,WORDLENGHT);
	
	
	char msg[WORDLENGHT];
    if(read(fd2[LEITURA],msg,WORDLENGHT)!= 0){//reads file
	
        printf("PAI: %s\n",msg);
    }	
    close(fd[LEITURA]);
    return 0;
}






