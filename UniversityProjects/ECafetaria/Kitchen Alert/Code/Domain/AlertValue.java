/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.booking;

import eapli.framework.domain.ddd.ValueObject;
import javax.persistence.Embeddable;

/**
 * The Kitchen Alert Value, can either be YELLOW = 0, or RED = 1;
 * @author Robin
 */
@Embeddable
public class AlertValue implements ValueObject{
    
    private int alertValue;
    /**
     * For ORM Purpose Only
     */
    protected AlertValue() {
    }

    public AlertValue(int alertValue) {
        if(alertValue < 2){
            this.alertValue = alertValue;
        } else {
        this.alertValue = 1;        
        }
    }
    /**
     * Writes down what alert it is depending on its value.
     * @return 
     */
    @Override
    public String toString() {
        if(alertValue == 1){
            return "RED";
        }         
        return "YELLOW";
    }

    
    
    
    
    
}
