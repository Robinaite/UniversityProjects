/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.isep.nsheets.server.lapr4.red.s2.ipc.n1120323.workbooks.application;

import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import pt.isep.nsheets.shared.core.Workbook;
import pt.isep.nsheets.shared.services.WorkbookDTO;
import org.xml.sax.InputSource;
import org.w3c.dom.*;
import pt.isep.nsheets.server.lapr4.white.s1.core.n4567890.workbooks.application.WorkbookService;
import pt.isep.nsheets.server.lapr4.white.s1.core.n4567890.workbooks.persistence.PersistenceContext;
import pt.isep.nsheets.server.lapr4.white.s1.core.n4567890.workbooks.persistence.WorkbookRepository;
import pt.isep.nsheets.shared.core.Spreadsheet;
import pt.isep.nsheets.shared.core.SpreadsheetImpl;
import pt.isep.nsheets.shared.core.Value;
import pt.isep.nsheets.shared.core.formula.green.s2.n1150461.GlobalVariable;
import pt.isep.nsheets.shared.core.lapr4.blue.s1.n1151314.macro.Macro;

/**
 *
 * @author David +  robin couwenberg
 */
public class ImportXmlController {
    
     private Document doc;
    private Element workbookElement;

    public ImportXmlController(String content) {
         DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db;
         try {
             db = dbf.newDocumentBuilder();
               InputSource is = new InputSource();
               String text = content.replace("\n", "").replace("\r", "");
            is.setCharacterStream(new StringReader(text));
            doc = db.parse(is);
         } catch (ParserConfigurationException | SAXException | IOException ex) {
             
         }
          
    }   

    public Workbook importXML(Workbook workBook, boolean append) {        
        NodeList workbookNode = doc.getElementsByTagName("Workbook");
        workbookElement = (Element) workbookNode.item(0);
        if(workbookNode.getLength()>0){
            //Work with workbook
            System.out.println("Importing Workbook!");
            Workbook wb = importWorkBook(workBook, append);
            final WorkbookRepository workbookRepository = PersistenceContext.repositories().workbooks();
            try {
                workbookRepository.save(wb);
            } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
                System.out.println("Errors");
            }
                return wb;

        } else {
            workbookElement = null;
            if(append){
                workBook.addSpreadsheet();
                importSpreadsheet(workBook.getSpreadsheet(workBook.getSpreadsheetCount()-1), append, 0);
            } else {
                 importSpreadsheet(workBook.getSpreadsheet(0), append, 0);
            }
           
                        final WorkbookRepository workbookRepository = PersistenceContext.repositories().workbooks();
            try {
                workbookRepository.save(workBook);
            } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
                System.out.println("Errors");
            }
                return workBook;
        }
    }
    
    private Workbook importWorkBook(Workbook workBook, boolean append){
        
        String wbName = workbookElement.getElementsByTagName("title").item(0).getTextContent();
        String wbDesc = workbookElement.getElementsByTagName("description").item(0).getTextContent();

        NodeList spreadsheetNodes = workbookElement.getElementsByTagName("spreadsheet");
        Workbook wb = new Workbook(spreadsheetNodes.getLength());
    
        wb.setName(wbName);
        wb.setDescription(wbDesc);
        
        if(append){
            int initialWorkbookCount = workBook.getSpreadsheetCount();
            for(int i = initialWorkbookCount;i<spreadsheetNodes.getLength()+initialWorkbookCount;i++){
                workBook.addSpreadsheet();
                System.out.println("Importing Spreadsheets!" + initialWorkbookCount + " i " + i);
                importSpreadsheet(workBook.getSpreadsheet(i), append,i-initialWorkbookCount);
            }
                importMacros(workBook,append);
                importGlobalVars(workBook, append);
            return workBook;
        } else {
            for(int i = 0;i<spreadsheetNodes.getLength();i++){
                System.out.println("Importing Spreadsheets!");
                importSpreadsheet(wb.getSpreadsheet(i), append,i);
            }
            importMacros(wb,append);
            importGlobalVars(wb, append);
            return wb;
        }
    }
    
    private void importSpreadsheet(Spreadsheet sp, boolean append, int numberSpreadsheet){
        Element spreadSheetElem;
        if(workbookElement == null){
            NodeList spreadsheetList = doc.getElementsByTagName("spreadsheet");
            spreadSheetElem = (Element) spreadsheetList.item(0);
        } else {
            NodeList spreadsheetList = workbookElement.getElementsByTagName("spreadsheet");
            System.out.println("Number Spreadsh: " + numberSpreadsheet);
            spreadSheetElem = (Element) spreadsheetList.item(0);
        }
        
        NodeList rows = spreadSheetElem.getElementsByTagName("row");
        NodeList cellsTemp = rows.item(0).getChildNodes();
        
        String[][] content = new String[rows.getLength()][cellsTemp.getLength()];
        
        for(int i = 0; i<rows.getLength();i++){
            NodeList cells = rows.item(i).getChildNodes();
            for(int j = 0;j<cells.getLength();j++){
                content[i][j] = cells.item(j).getTextContent();
            }
        }
        sp.changeContent(content, append);
    }

    private void importMacros(Workbook workBook,boolean append) {
        
        NodeList macros = workbookElement.getElementsByTagName("macros");
        Element macrosTag = (Element) macros.item(0);
       
        NodeList macrosList = macrosTag.getElementsByTagName("macro");
        
        if(macrosList.getLength()!=0){
            if(append){
                for(int i = 0; i<macrosList.getLength();i++){
                    Element macro = (Element) macrosList.item(i);
                    String macroName = macro.getElementsByTagName("name").item(0).getTextContent();
                    String macroContent = macro.getElementsByTagName("content").item(0).getTextContent();
                    Macro m = new Macro(macroName, macroContent);
                    //No method to add new macros to the workbook
                }
            } else {
                for(int i = 0; i<macrosList.getLength();i++){
                    Element macro = (Element) macrosList.item(i);
                    String macroName = macro.getElementsByTagName("name").item(0).getTextContent();
                    String macroContent = macro.getElementsByTagName("content").item(0).getTextContent();
                    Macro m = new Macro(macroName, macroContent);
                    //No method to substitute the macros to the workbook
                }
            }
        }        
    }
    private void importGlobalVars(Workbook workBook,boolean append) {
        
        NodeList globalVars = workbookElement.getElementsByTagName("globalVars");
        Element globalVarsTag = (Element) globalVars.item(0);
       
        NodeList varList = globalVarsTag.getElementsByTagName("var");
        
        if(varList.getLength()!=0){
            if(append){
                for(int i = 0; i<varList.getLength();i++){
                    Element macro = (Element) varList.item(i);
                    String varName = macro.getElementsByTagName("varName").item(0).getTextContent();
                    String varContent = macro.getElementsByTagName("varContent").item(0).getTextContent();
                    GlobalVariable var = new GlobalVariable(varName);
                    var.updateValue(new Value(varContent));
                    workBook.addNewGlobalVariable(var);
                }
            } else {
                for(int i = 0; i<varList.getLength();i++){
                    Element macro = (Element) varList.item(i);
                    String varName = macro.getElementsByTagName("varName").item(0).getTextContent();
                    String varContent = macro.getElementsByTagName("varContent").item(0).getTextContent();
                    GlobalVariable var = new GlobalVariable(varName);
                    var.updateValue(new Value(varContent));
                    workBook.addNewGlobalVariable(var);
                }
            }
        }        
    }
}
