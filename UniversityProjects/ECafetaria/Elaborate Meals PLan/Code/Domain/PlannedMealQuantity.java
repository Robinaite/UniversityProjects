/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.kitchen;

import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 *
 * @author Robin Couwenberg
 */
@Embeddable
public class PlannedMealQuantity implements Serializable {
    
    private static final int DEFAULT_VALUE = 0;
    
    private int quantity;
    
    public PlannedMealQuantity() {
        this.quantity = DEFAULT_VALUE;
    }
    /**
     * Obtain integer of planned quantity
     * @return integer
     */
    public int obtainPlannedQuantity() {
        return quantity;
    }
    /**
     * Changes the planned quantity
     * @param qnt the quantity to change to;
     * @return true -> value was accepted, false -> not able to change the planned quantity
     */
    public boolean setPlannedQuantity(int qnt){
        if(validate(qnt)){
            quantity = qnt;
            return true;
        }
        return false;
    }
    /**
     * Validates the value
     * @param qnt -  the quantity to validate
     * @return true if valid, false if not valid.
     */
    private boolean validate(int qnt){
        return qnt > 0;
    }   

    @Override
    public String toString() {
        return "Planned Quantity: " + quantity;
    }
    
    
}
