/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.kitchen.MealsPlan;
import eapli.ecafeteria.persistence.MealsPlanRepository;
import java.util.Optional;

/**
 *
 * @author Diogo Barreto (1150468) + Robin Couwenberg(1161272)
 */
public class JpaMealsPlanRepository extends CafeteriaJpaRepositoryBase<MealsPlan, Long> implements MealsPlanRepository {

    @Override
    public Optional<MealsPlan> findByID(Long id) {
        return matchOne("e.id =:id", id);

    }

    @Override
    public Iterable<MealsPlan> findAllOpenPlans() {
        return match("e.status = 0");
    }

}
