This project was made for the EAPLI(Applications Engineering) where with the whole class constituted of about 20 students we needed to create a software for a Cafeteria. 

The subjects objectives are: 

1 - Understand and apply iterative and incremental software development process
2 - Work collaboratively within a software team
3 - Apply test driven methodologies, tools and techniques
4 - use revision control systems
5 - understand and apply object-relational mapping techniques and tools
6 - Understand and apply SOLID principles and GoF software design patterns
7 - Understand and apply Domain-Driven Design tactical patterns
8 - Apply the software development process to a medium/large project
9 - Using appropriate artefacts and notations, describe the involved proposals and solutions

As this project was made by the whole class we were needed to divide the various use cases. The use-cases that were implemented by myself are the following:

1. Elaborate Meals Plan
2. Kitchen Alert Generation

U can check the respective folders for more information about the use cases.

In the respective folder there is not all of the code that was written for the use cases due to it being in shared classes which were edited alot. So could only get snippets of it, maybe when I have time.

I ended the project with the evaluation of a 18 in 20.