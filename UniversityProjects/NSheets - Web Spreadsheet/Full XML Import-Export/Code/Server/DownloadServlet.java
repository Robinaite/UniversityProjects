/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.isep.nsheets.server.lapr4.blue.s3.ipc.n1161272.servlet;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Robin
 */
public class DownloadServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String fileId = request.getParameter("fileId"); // value of file id from request
        File file = getFile(fileId); // your method to create file from helper class

// setting response headers
        int BUFFER = 1024 * 100;
            response.setContentType( "application/octet-stream" );
            response.setHeader( "Content-Disposition:", "attachment;filename=" + "\"" + fileId + "\"" );
            ServletOutputStream outputStream = response.getOutputStream();
            response.setContentLength( Long.valueOf( file.length() ).intValue() );
            response.setBufferSize( BUFFER );

        BufferedInputStream input = null;
        BufferedOutputStream output = null;

        try {
            InputStream inputStream = new FileInputStream(file);

            input = new BufferedInputStream(inputStream);
            output = new BufferedOutputStream(outputStream);

            int count;
            byte[] buffer = new byte[8192]; //  buffer size is 512*16
            while ((count = input.read(buffer)) > 0) {
                output.write(buffer, 0, count);
            }

        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException ex) {
                }
            }
            if (input != null) {
                try {
                    input.close();
                } catch (IOException ex) {
                }
            }
        }
    }

    private File getFile(String fileId) {
        return new File(fileId);
    }

}
