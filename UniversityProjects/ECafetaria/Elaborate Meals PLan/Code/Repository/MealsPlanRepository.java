/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.kitchen.MealsPlan;
import eapli.framework.domain.Designation;
import eapli.framework.persistence.repositories.DataRepository;
import java.util.Optional;

/**
 *
 * @author Diogo Barreto(1150468)
 */
public interface MealsPlanRepository extends DataRepository<MealsPlan, Long>{
   
    Optional<MealsPlan> findByID(Long id);

    public Iterable<MealsPlan> findAllOpenPlans();
    
}
