/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.booking;

import eapli.framework.domain.ddd.DomainEventBase;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * The alert that will be generated whenever the % of bookings and meals plans go over a defined value
 * @author Robin Couwenberg (1161272)
 */
@Entity
public class KitchenAlert extends DomainEventBase{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long ID;
    
    private Accepted accepted;
    
    private AlertValue alertValue;

    /**
     * For ORM Only
     */
    protected KitchenAlert() {
    }
    /**
     * Creates an kitchen alert with the respective alert value
     * @param alertValue the alert value
     */
    public KitchenAlert(int alertValue) {
        super();
        this.accepted = new Accepted();
        this.alertValue = new AlertValue(alertValue);
    }
    
    /**
     * The alert was seen and accepted.
     */
    public void accept(){
        this.accepted.accept();
    }    
    /**
     * Checks if the alert is accepted
     * @return true if accepted, false if not.
     */
    public boolean isAccepted(){
        return this.accepted.isAccepted();
    }
    /**
     * The ID of the Kitchen alert
     * @return  The ID of the Kitchen alert
     */
    public Long id() {
        return ID;
    }
    /**
     * To String of the KitchenAlert
     * @return an string of the kitchenAlert
     */
    @Override
    public String toString() {
        return "Alert: " + alertValue.toString() + " Date: " + occurredAt().getTime().toString();
    }
    

    
}
