/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.booking;

import eapli.framework.domain.ddd.ValueObject;
import javax.persistence.Embeddable;

/**
 *
 * @author Robin
 */
@Embeddable
public class Accepted implements ValueObject{
    
    private boolean accepted;
    /**
     * Constructor of accepted, initializes as false;
     */
    public Accepted() {
        this.accepted = false;
    }
    /**
     * Accept (accepted = true)
     */
    public void accept(){
        this.accepted = true;
    }
    /**
     * checks if accepted
     * @return accepted
     */
    public boolean isAccepted(){
        return accepted;
    } 
}
